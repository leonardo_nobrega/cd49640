
h2. Introduction

This document is a collection of ideas that should help with the implementation of the combined metrics feature in analytics.

It gives a high-level description of the data structure and algorithm and the outline of a decomposition into modules, based on the constraints derived from the feature specification and the current state of the analytics components. It does not contain a detailed description on the feature's implementation. By the time it was written, we had not decided yet which libraries or platforms we would use.

The document may sometimes refer to combined metrics as derived metrics.

h2. Definitions

Combined metrics configuration: an instance of a derived SIM type that defines a combined metric.

Grace period: the time interval the system waits for inputs metrics before loging an error.

h2. Core points

Some of these are direct consequence of points in the specification, some of them are consequence of the way analytics is at the moment, some are simply obvious.

1. The input of the combined metrics module will either be the short-term table in Cassandra or the analytics Kafka topic (this is where input-metrics are).

2. The combined metrics will go to the short-term table (because the table is the input for squares and aggregator) and to the TCA module.

This is because combined metrics will serve as input to all existing modules in analytics, just like regular metrics.

3. The combined metrics job is periodic. Its frequency is equal to or less than the grace period.

Because the system constantly receives samples from the customer network.

(There is here an implicit assumption that the input for the combined metrics engine will be in the form of batches. In the system's current configuration, the spark-ingest engine processes micro-batches of samples.)

4. Before the computation of each derived metric, there must be a filtering step that selects the input metrics from the pool of samples that analytics receives.

Not all samples will be used to compute a combined metric.

5. Combined metrics are associated to network objects (routers, vms, etc).

See Serge's second answer \[CMT2].

6. All metrics appearing in a formula belong to the same network object. In other words, the samples providing the input metrics have the same id.

See Serge's first comment \[CMT1].

7. All input samples have timestamps within a time interval of a fixed duration, and the time intervals corresponding to two consecutive combined metrics do not overlap.

See Serge's second comment \[CMT2].

8. A formula for a combined metric will always return a real number.

See Serge's first comment \[CMT1].

h2. Pending computations

1. Because the grace period can be larger than the job frequency, there needs to be a record of what metrics could not be calculated due to missing inputs.

The pending computations state (PCS) is a data structure linking combined metrics to input metrics.

2. The input metrics appearing in the PCS will be the set of distinct input metrics appearing in all the combined metrics configuration objects.

3. After filtering the sample pool to obtain new input metrics, the combined metrics engine puts them onto the PCS.

4. The combined metrics engine will query the PCS to log error messages for metrics that could not be computed because of missing inputs.

5. The PCS may grow in size and exceed the available memory. Suppose derived metric C uses input metrics A and B, metric A has high frequency, the system stops receiving metric B and the grace period is very long. The PCS would exhaust the host's memory with A values that will not be used, because the B values are missing.

6. A way to implement the PCS is as a key-value collection. The key is a tuple containing the combined metric name, the sample id and timestamp, and the value is a tuple containing the values of the input metrics.

{code}
--- K ----  ---- V ----
             A  B
[cm id ts]  [v1 v2 ...]
{code}


7. A variation of the key-value collection suggested by Mostefa uses the combined metric name and sample id for the key (no timestamp) and pairs of timestamp and value for each input metric. When the system receives a new sample with an id that is already stored in the PCS, the engine replaces the existing timestamp-value pair with a new one containing the new sample's timestamp and value.

{code}
-- K ---   -------- V --------
             A      B
[cm id]    [[ts v] [ts v] ...]
{code}

This has the benefit of less memory usage and leads to an implementation similar to the current TCA. But it compromises the ability to generate missing input logs. It will also fail to calculate combined metrics if given an input consisting of sequences of samples having the same metric:

combined metric C = A + B

|| sample || metric || ts ||                         ||
 | 1       | A       | 0   | overwritten by sample 2  |
 | 2       | A       | 1   | overwritten by sample 3  |
 | 3       | A       | 2   | overwritten by sample 4  |
 | 4       | A       | 3   | used with sample 8       |
 | 5       | B       | 0   | overwritten by sample 6  |
 | 6       | B       | 1   | overwritten by sample 7  |
 | 7       | B       | 2   | overwritten by sample 8  |
 | 8       | B       | 3   | used with sample 4       |

h2. Errors

1. There are three kinds of errors: missing samples, exceptions during computation and periodicity mismatches.

2. At the end of the grace period, the combined metrics engine logs the missing samples errors.

3. Once the engine prints an error log, it will omit any errors for the same combined metric and id until the next hour or until it successfully computes the combined metric.

4. The engine should maintain an error state that will let it know when to discard an error log. The error state may be a set of records containing (1) an id, (2) a combined metric name, (3) the error message, (4) the time when the engine logged the message.

5. Flooding would still be possible if the error state discards logs with the same id, metric and error message. For example, a formula that throws an exception, applied once to input metrics from 10,000 different ids would generate as many error messages that would not be discarded, because they all have a unique id.

It might be good to have a maximum number of error logs associated to the log level. For example, trace logs would be unrestricted, there would be at most 100 debug logs and at most 10 info logs.

h2. Combined metric attributes

||   || attr name                  || type           || cardinality ||
 | 1  | combined metric name        | string          | 1            |
 | 2  | list of input metric names  | list of string  | 1..N         |
 | 3  | frequency in seconds        | int             | 1            |
 | 4  | function                    | source code     | 1            |
 | 5  | sim-type                    | string          | 1            |
 | 6  | sim-instance                | string          | 0..1         |

h2. Association between combined metric and network-object sim-instance

If combined metrics is implemented as a batch process, it can use the association to discard samples that are not input to any combined metric, at the cost of looking up the SIM type to which the sample id belongs.

h2. References

\[SPEC] Jira ticket containing the specification

https://cenx-cf.atlassian.net/browse/CD-40831

\[CMT1] Serge's comment that confirms (1) the value of a combined metric is always a number and (2) all input metrics of a combined metric have the same id.

https://cenx-cf.atlassian.net/browse/CD-40831?focusedCommentId=164871&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-164871

\[CMT2] Serge's comment that says (1) a combined metric is associated to a SIM type and (2) descriptors define the time interval for collection of input metrics.

https://cenx-cf.atlassian.net/browse/CD-40831?focusedCommentId=165675&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-165675

\[] My notes file, the source for most of what is written here. As I worked, I added notes without going back to check for consistency with the existing content very often.

https://bitbucket.org/leonardo_nobrega/cd49640/src/master/doc/notes.txt