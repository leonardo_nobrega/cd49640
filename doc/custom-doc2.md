
This describes how to create an analytics combined metric in the system.

h2. Descriptor

The redify code for descriptors in docker-nifi is now capable of processing a combined-metrics attribute. This is a list of JSON objects. Each object should contain a name, a list of input metrics, an associated type and a formula.

{code}
"combined-metrics" : [ {
  "name" : "FailureRatio",

  "//" : "The id in the input-metric should match the id of an object in",
  "//" : "monitoring-params. The resulting input-metric will receive",
  "//" : "the attributes of the corresponding monitoring-param.",
  "input-metrics" : [ {"id" : "total"}, {"id" : "success"} ],
  "associated-type" : "VirtualMachine",
  "formula" : "function (total, success) { return (total - success) / total; }"
},
{
  "name" : "AverageOfThree",
  "input-metrics" : [ {"id" : "metric1"}, {"id" : "metric2"},
                      {"id" : "metric3"} ],
  "associated-type" : "Site",
  "formula" : "function (metric1, metric2, metric3) { return (metric1 + metric2 + metric3) / 3; }"
} ]
{code}

The associated type points to a RED object representing a network entity, defined in red/model.js The connection between a combined metric and the network entity is created by a directive in red/links.js. See [Customization Documents|#CustomizationDocuments] below.

h3. Monitoring Parameters, Input Metrics and Formula Parameters

{code}
"monitoring-params" : [ {
  "collection-period" : "60",
  "id" : "total"
},
{
  "collection-period" : "60",
  "id" : "success"
},
{
  "collection-period" : "60",
  "id" : "metric1"
},
{
  "collection-period" : "60",
  "id" : "metric2"
},
{
  "collection-period" : "60",
  "id" : "metric3"
} ]
{code}

The descriptor contains a monitoring-params attribute. This is not a new attribute, it existed before the combined metrics changes. It is a "sibling" of combined-metrics (they are both attributes of the same JSON object). Monitoring-params is a list of objects. Each monitoring-param object must contain two attributes: id and collection-period.

Input-metrics is an attribute of a combined-metric object. Input-metrics is a list of objects. Currently, each input-metric object must provide only an id.

Formula is also an attribute of a combined-metric object. It contains javascript code for a function that computes the combined metric from values of its input metrics. The function has a list of parameters; these are the formula parameters.

* Each formula parameter must have a corresponding input-metric object and a corresponding monitoring-param object. The relationship is based on the equality of the formula parameter and the id attributes of the input-metric and monitoring-param.

* The number of input-metrics and the number of parameters in the formula must be equal.

* The monitoring-param associated to the formula parameter must provide a collection-period attribute. All collection periods of the monitoring-params associated to a formula must be equal.

In the example above, total and success are parameters for FailureRatio. Metric1, metric2 and metric3 are parameters for AverageOfThree.

h2. Customization Documents

h3. red/model.js
{code}
{
  "id": "combinedMetric",
  "directiveType": "ENTITY",
  "target": "RED",
  "directiveVersion": "2.0",
  "payload": {
    "type": "CombinedMetric",
    "properties": {
      // concatenation of the combined metric name and the associated type
      "id": { "type": "string", "required": true },

      // type is CombinedMetric (set by the redify code in docker-nifi)
      "type": { "type": "string" },

      "name": { "type": "string", "required": true },

      // SIM type on which the combined metric is defined
      "associatedType": { "type": "string", "required": true },

      // array of objects
      // each object contains attributes (all are strings):
      // id, collection-period
      "inputMetrics": { "type": "array", "required": true },

      "formula": { "type": "string", "required": true }
    }
  }
}
{code}

This is a schema for the combined metric RED object.

h3. red/links.js
{code}
{
  "id": "VirtualMachineHasCombinedMetric",
  "directiveType": "LINK",
  "target": "RED",
  "directiveVersion": "2.0",
  "payload": {
    "type": "VirtualMachineHasCombinedMetric",
    "description": "A combined metric associated to the virtual machine",

    "from": { "type": "VirtualMachine", "property": "_type" },
    "to": { "type": "CombinedMetric", "property": "associatedType" }
  }
}
{code}

This creates links between RED objects representing combined metrics and RED objects for network entities (in the example, virtual machines).

Both objects in the link provide names of properties; see the "from" and "to" attributes in the directive above. The link is made when the values of the properties match.

h3. sim/derived.js
{code}
{
  "id": "CombinedMeasurement",
  "directiveType": "ENTITY",
  "target": "SIM",
  "directiveVersion": "2.0",
  "payload": {
    "type": "CombinedMeasurement",
    "derivedFrom": "SIMMeasurement",
    "properties": {
      "name": { "type": "string", "required": true },

      // reference to the SIM instance
      "associatedSimInstance": { "type": "string", "required": true },

      // array of objects
      // each object contains attributes:
      // id (string), collectionPeriod (int)
      "inputMetrics": { "type": "array", "required": true },

      // javascript source
      "formula": { "type": "string", "required": true }
    }
  }
}
{code}

Schema for the combined metric SIM object. One important difference between the RED and SIM objects: the associated SIM instance attribute here points to a single network entity. In the RED object, the associated type points to a kind of network entity.

h3. sim/mapping.js
{code}
{
  // We intend here to generate a combined metric config object for each vm.
  "id": "vm-combined-metric",
  "directiveType": "MAPPING",
  "target": "SIM",
  "directiveVersion": "2.0",
  "payload": {
    "type": "tree-walk",
    "name": "vm-combined-metric",
    "anchor": "VirtualMachine",
    "gatherFn": [
      // save the vm id in the path

      // add is defined in
      // cenx-platform/cenx-management/resources/mappingFns.js
      add("_vmId", "_id")
    ],
    "walk": {
      // relationship defined in red/links.js
      "VirtualMachineHasCombinedMetric": {
        // save the combined metric (vertex) in the path
        "gatherFn": function (path, vertex) {
          path["_redCombinedMetric"] = vertex;
          return path;
        }
      }
    },
    "generateFn": function (path) {
      var combinedMetric = {};
      var red = path._redCombinedMetric;
      // do not continue without the RED object
      // (it will be absent if the system didn't ingest a descriptor)
      if (!red) {
        return;
      }

      // combined metric name
      combinedMetric.name = red.name;

      // id of the associated SIM instance (the vm in this example)
      // this gives analytics a way to route samples to input-metrics buckets
      combinedMetric.associatedSimInstance = path._vmId;

      // list of input metrics
      // - id is the sample's metric name
      // - collectionPeriod is in seconds
      combinedMetric.inputMetrics = red.inputMetrics.map(function (redInput) {
        var simInput = {};
        simInput.id = redInput["id"];
        simInput.collectionPeriod = redInput["collection-period"];
        return simInput;
      });

      // formula
      combinedMetric.formula = red.formula;

      // required SIMMeasurement attributes
      combinedMetric._type = "CombinedMeasurement";
      combinedMetric._id = combinedMetric.associatedSimInstance + combinedMetric.name;

      return combinedMetric;
    }
  }
}
{code}

The generate function (generateFn) in this directive creates the SIM object that Levski needs to compute combined metrics.

The add instruction in the gatherFn attribute stores the virtual machine's id in the path object as the _vmId attribute. GenerateFn sets the combined metric's associatedSimType to be the value of _vmId. The path object is a kind of accumulator.

The directive's walk attribute associates the name of the relationship defined in red/links.js (VirtualMachineHasCombinedMetric) to a javascript function. The function creates a _redCombinedMetric attribute in the path object to store the object at the other end of the link (the combined metric). GenerateFn uses _redCombinedMetric to populate the fields of the SIM combined metric.

h2. References

Cenx-pseudotel PR:
https://bitbucket.org/cenx-cf/cenx-pseudotel/pull-requests/210

Docker-nifi PR:
https://bitbucket.org/cenx-cf/docker-nifi/pull-requests/152
