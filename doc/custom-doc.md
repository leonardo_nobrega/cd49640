
Below is a PseudoVM mapping directive modified to create a combined-metric SIM entity.

{code}
  {
    "id": "vm",
    "directiveType": "MAPPING",
    "target": "SIM",
    "directiveVersion": "2.0",
    "payload": {
      "type": "tree-walk",
      "name": "vm",
      "anchor": "VirtualMachine",
      "gatherFn": [
        // no changes here
      ],
      "walk": {
        // no changes here
      },
      "generateFn": [
        // We intend here to generate a combined metric config object for each vm.
        function (path) {
          var combinedMetric = {};

          // combined metric name
          combinedMetric.name = "FailureRatio";

          // id of the associated SIM instance (the vm in this example)
          // this gives analytics a way to route samples to input-metrics buckets
          combinedMetric.associatedSimInstance = path._id;

          // list of input metrics
          // - id is the sample's metric name
          // - collectionPeriod is in seconds
          // - it should be possible to populate this attribute using
          //   an ingested descriptor
          combinedMetric.inputMetrics = [
            {"id": "total",
             "collectionPeriod": 60},
            {"id": "success",
             "collectionPeriod": 60}
          ];

          // formula
          combinedMetric.formula =
              "function (total, success) { return (total - success) / total; }";

          // required SIMMeasurement attributes
          combinedMetric._type = "CombinedMeasurement";
          combinedMetric._id =
              combinedMetric.associatedSimInstance + combinedMetric.name;

          return combinedMetric;
        }
      ]
    }
  }
{code}

The definition for the SIM derived type CombinedMeasurement will be in sim/derived.js.

{code}
  {
    "id": "CombinedMeasurement",
    "directiveType": "ENTITY",
    "target": "SIM",
    "directiveVersion": "2.0",
    "payload": {
      "type": "CombinedMeasurement",
      "derivedFrom": "SIMMeasurement",
      "properties": {
        "name": {
          "type": "string",
          "required": true
        },
        "associatedSimInstance": {
          // reference to the SIM instance
          "type": "string",
          "required": true
        },
        "inputMetrics": {
          // array of objects
          // each object contains attributes:
          // id (string), collectionPeriod (int)
          "type": "array",
          "required": true
        },
        "formula": {
          // javascript source
          "type": "string",
          "required": true
        }
      }
    }
  }
{code}

On Cassandra:

!query.png!

For a given SIM type (vm in this example), all CombinedMeasurement attributes excluding _id and associatedSimInstance will be the same. One way to avoid repeating these values is to have an association object that would contain a reference to the combined metric (an id or name) and an id for the SIM instance.

It is possible to define combined metrics in TOSCA descriptors. See [Defining Combined Metrics]
