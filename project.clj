(defproject cd49640 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.apache.kafka/kafka-streams "1.1.0"]
                 [org.apache.kafka/kafka-clients "1.1.0"]
                 [org.slf4j/slf4j-simple "1.7.25"]
                 ]
  :aot :all
  :java-source-paths ["src/cd49640"]
  :profiles {:repl {:main cd49640.core
                    :target-path "target"}}
  )
