package cd49640;

import clojure.lang.IPersistentMap;
import clojure.lang.Keyword;

public class Sample {
    public String id;
    public String metric;
    public Long timestamp;
    public Double value;

    public Sample() {
    }

    public Sample(String id, String metric, Long timestamp, Double value) {
        this.id = id;
        this.metric = metric;
        this.timestamp = timestamp;
        this.value = value;
    }

    public String toString() {
        return "{" +
            "id:" + id + ", " +
            "metric:" + metric + ", " +
            "timestamp:" + timestamp + ", " +
            "value:" + value +
            "}";
    }

    // (cd49640.Sample/fromClojureMap {:id "a" :metric "m" :timestamp 0 :value 1.0})
    public static Sample fromClojureMap(IPersistentMap map) {
        Sample s = new Sample();
        s.id = (String) map.entryAt(Keyword.find("id")).val();
        s.metric = (String) map.entryAt(Keyword.find("metric")).val();
        s.timestamp = (Long) map.entryAt(Keyword.find("timestamp")).val();
        s.value = (Double) map.entryAt(Keyword.find("value")).val();
        return s;
    }
}
