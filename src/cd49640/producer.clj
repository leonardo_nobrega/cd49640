(ns cd49640.producer
  (:import cd49640.Sample
           com.fasterxml.jackson.databind.ObjectMapper
           [org.apache.kafka.clients.producer
            KafkaProducer ProducerRecord]))

#_(Class/forName "cd49640.producer__init")

(def object-mapper (new ObjectMapper))

(defn make
  ([topic]
   (let [serializer "org.apache.kafka.common.serialization.StringSerializer"]
     (make topic serializer serializer)))
  ([topic k-serializer v-serializer]
   {:producer (new KafkaProducer
                   ;; config
                   (doto (new java.util.Properties)
                     (.put "bootstrap.servers" "localhost:29092")
                     (.put "key.serializer" k-serializer)
                     (.put "value.serializer" v-serializer)))
    :topic topic}))

(defn push-kv
  [producer k v]
  (-> producer
      :producer
      (.send (new ProducerRecord (:topic producer) k v))
      ;; java.util.concurrent.Future
      (.get)))

(defn push
  [producer sample]
  (push-kv producer nil (.writeValueAsString object-mapper sample)))

(defn flush
  [producer]
  (-> producer :producer .flush))

(defn close
  [producer]
  (-> producer :producer .close))

