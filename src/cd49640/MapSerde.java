package cd49640;

import java.util.Map;

public class MapSerde extends JsonSerde<Map> {
    public MapSerde() {
        super(Map.class);
    }
}
