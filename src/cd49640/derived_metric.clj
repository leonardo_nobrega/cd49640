(ns cd49640.derived-metric
  (:require [clojure.set :as clj-set])
  (:import [cd49640 Sample]))

#_(Class/forName "cd49640.derived_metric__init")

#_(let [dm (cd49640.derived-metric/make "c"
                                      ["a" "b"]
                                      "function c(a, b) { return a + b; }")
      samples [{"id" "id0" "metric" "a" "timestamp" 0 "value" 10.0}
               {"id" "id0" "metric" "b" "timestamp" 0 "value" 1.0}]]
  (cd49640.derived-metric/compute dm samples))

(defn- eval-js
  [js-src]
  (let [script-engine (-> (javax.script.ScriptEngineManager.)
                          (.getEngineByName "nashorn"))
        make-clj-fn (fn [js-fn]
                      (fn [& args]
                        (.call js-fn nil (into-array Object args))))]
    (-> script-engine
        ;; returns an instance of
        ;; jdk.nashorn.api.scripting.ScriptObjectMirror
        (.eval js-src)
        ;; wraps a clojure fn around the ScriptObjectMirror
        make-clj-fn)))

(defn make
  [derived-metric-name input-metric-names js-function-src]
  {:name derived-metric-name
   :inputs (set input-metric-names)
   :src js-function-src
   :fn (eval-js js-function-src)})

(defn derived-metric-timestamp
  [input-samples]
  ;; TODO truncate based on the duration of the time-bucke
  ;; or something else
  (-> input-samples
      first
      (.get "timestamp")
      long))

;; returns a Sample
(defn compute
  [derived-metric input-samples]
  (let [sample (first input-samples)
        id (.get sample "id")
        metric-name (:name derived-metric)
        timestamp (derived-metric-timestamp input-samples)
        input-metrics (->> input-samples (map #(.get % "metric")) set)]
    (if (= input-metrics (:inputs derived-metric))
      (as-> input-samples $
        (map #(.get % "value") $)
        (apply (:fn derived-metric) $)
        (new Sample id metric-name timestamp $))
      {"id" id
       "timestamp" timestamp
       "missing" (clj-set/difference (:inputs derived-metric)
                                     input-metrics)})))

(defn get-inputs
  [derived-metric]
  (-> derived-metric :inputs set))

#_(cd49640.derived-metric/is-input?
 (cd49640.derived-metric/make "c"
                              ["a" "b"]
                              "function c(a, b) { return a + b; }")
 (Sample/fromClojureMap {:id "i" :metric "a" :timestamp 0 :value 1.0}))

(defn is-input?
  [derived-metric sample]
  ;; (:inputs derived-metric) is a set
  (boolean ((:inputs derived-metric)
            (.get sample "metric"))))
