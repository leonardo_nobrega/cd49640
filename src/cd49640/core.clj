(ns cd49640.core
  (:require [cd49640.derived-metric :as dm])
  (:import [cd49640 MapSerde Sample]
           [org.apache.kafka.common.serialization Serdes Serdes$StringSerde]
           [org.apache.kafka.streams
            Consumed KafkaStreams StreamsBuilder StreamsConfig
            Topology Topology$AutoOffsetReset]
           [org.apache.kafka.streams.kstream
            Aggregator Initializer KeyValueMapper Materialized
            Predicate Printed ValueMapper]))

;; confluent page on how to use the kafka docker image
;; https://docs.confluent.io/current/installation/docker/docs/quickstart.html

;; list topics
;; docker-compose exec kafka kafka-topics --list --zookeeper 172.17.0.1:32181

;; create topic in-test:
;; docker-compose exec kafka \
;; kafka-topics --create --topic in-test --partitions 1 --replication-factor 1 \
;; --if-not-exists --zookeeper 172.17.0.2:32181

;; write a sample to topic in-test
;; docker-compose exec kafka bash -c \
;; "echo '{\"id\":\"123\", \"metric\":\"cpu\", \"timestamp\":1234, \"value\":10.0}' | \
;; kafka-console-producer --request-required-acks 1 --broker-list 172.17.0.1:29092 \
;; --topic in-test && echo 'done'"

;; ubuntu usage
;; 1. install openjdk 8:
;;   sudo apt-get install openjdk-8-jdk
;; 2. select it:
;;   sudo update-alternatives --config java

(defn consumer-config
  []
  (Consumed/with
   ;; key serializer/deserializer
   (Serdes/String)

   ;; value serializer/deserializer
   (new MapSerde)

   ;; timestamp extractor
   ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams
   ;; /processor/TimestampExtractor.html
   nil

   ;; reset policy
   Topology$AutoOffsetReset/LATEST))

;; returns an org.apache.kafka.streams.kstream.Predicate
(defn derived-metric->predicate
  [derived-metric]
  (proxy [Predicate] []
    (test [k v]
      (dm/is-input? derived-metric v))))

(defn key-from-id-and-timestamp
  [id timestamp]
  (str id timestamp))

(def kv-mapper-for-group-by
  (proxy [KeyValueMapper] []
    (apply [k v] (key-from-id-and-timestamp (.get v "id")
                                            (.get v "timestamp")))))

(def initial-value
  (proxy [Initializer] []
    ;; empty map
    (apply [] (new java.util.TreeMap))))

(defn- make-input-metrics-aggregator
  [derived-metric]
  ;; TODO use the inputs in the derived-metric
  (proxy [Aggregator] []
    (apply [k v aggregate]
      (.put aggregate (.get v "metric") v)
      aggregate)))

;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/kstream
;; /Materialized.html
(def input-metrics-materialized-store
  (doto (Materialized/as "input-metrics")
    (.withKeySerde (Serdes/String))
    (.withValueSerde (new MapSerde))))

(defn make-value-mapper-to-compute-derived-metrics
  [derived-metric]
  (proxy [ValueMapper] []
    (apply [v]
      ;; v is a LinkedHashMap: metric-name -> sample
      (->> v
           .values
           seq
           (dm/compute derived-metric)))))

(def derived-metric-materialized-store
  (doto (Materialized/as "derived-metric")
    (.withKeySerde (Serdes/String))
    (.withValueSerde (new MapSerde))))

(def stream-printer
  ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/kstream
  ;; /Printed.html
  (doto (Printed/toSysOut)
    (.withKeyValueMapper (proxy [KeyValueMapper] []
                           (apply [k v] (str "k:\"" k "\" v:\"" v "\""))))))

;; input-stream is an org.apache.kafka.streams.kstream.KStream
(defn set-up-derived-metric-computation
  [derived-metric input-stream]
  (-> input-stream
      ;; group-by id and timestamp
      ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/kstream
      ;; /KStream.html#groupBy
      (.groupBy kv-mapper-for-group-by)
      ;; org.apache.kafka.streams.kstream.KGroupedStream
      (.aggregate initial-value
                  (make-input-metrics-aggregator derived-metric)
                  input-metrics-materialized-store)
      ;; org.apache.kafka.streams.kstream.KTable
      ;; calculate
      ;; it seems KTables have no ttl, the app must explicitly delete records
      ;; https://stackoverflow.com/questions/48080721
      ;; /kafka-streams-ktable-from-topic-with-retention-policy?rq=1
      (.mapValues (make-value-mapper-to-compute-derived-metrics derived-metric)
                  derived-metric-materialized-store)
      (.toStream)
      (.print stream-printer)
      ))

(defn streams-builder
  [input-topic derived-metrics]
  ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/StreamsBuilder.html
  ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/kstream
  ;; /KStream.html
  (let [builder (new StreamsBuilder)
        predicates (map derived-metric->predicate derived-metrics)
        input-streams (-> builder
                          (.stream input-topic (consumer-config))
                          (.branch (into-array Predicate predicates)))]
    (->> input-streams
         (map set-up-derived-metric-computation derived-metrics)
         dorun)
    builder))

(defn topology
  [input-topic derived-metrics]
  (.build (streams-builder input-topic derived-metrics)))

;; https://kafka.apache.org/11/documentation/streams/developer-guide/config-streams
(defn streams-config
  []
  ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/StreamsConfig.html
  (new StreamsConfig
       (doto (new java.util.Properties)
         (.put StreamsConfig/APPLICATION_ID_CONFIG "cd49640")
         (.put StreamsConfig/BOOTSTRAP_SERVERS_CONFIG "localhost:29092")
         (.put StreamsConfig/DEFAULT_KEY_SERDE_CLASS_CONFIG Serdes$StringSerde)
         (.put StreamsConfig/DEFAULT_VALUE_SERDE_CLASS_CONFIG MapSerde)
         )))

(defn exception-handler
  []
  (proxy [Thread$UncaughtExceptionHandler] []
    (uncaughtException [thread throwable]
      (println "exception-handler" throwable))))

;; kafka-streams -> topology -> streams-builder -> consumer-config
;;               -> streams-config
;;               -> exception-handler
(defn kafka-streams
  [input-topic derived-metrics]
  ;; https://kafka.apache.org/10/javadoc/org/apache/kafka/streams/KafkaStreams.html
  (doto (new KafkaStreams
             (topology input-topic derived-metrics)
             (streams-config))
    (.setUncaughtExceptionHandler (exception-handler))))

(defn add-shutdown-hook
  [streams]
  (.addShutdownHook (Runtime/getRuntime)
                    (Thread. (fn [] (.close streams)))))

(defn start-kafka-streams
  [streams]
  (.start streams))

(defn close-kafka-streams
  [streams]
  (.close streams))

(comment
  ;; create derived metric
  (Class/forName "cd49640.derived_metric__init")
  (def dm (cd49640.derived-metric/make "c"
                                       ["a" "b"]
                                       "function c(a, b) { return a + b; }"))

  ;; start kafka-streams
  (def streams (kafka-streams "in-test" [dm]))
  (.start streams)

  ;; create producer
  (Class/forName "cd49640.producer__init")
  (def prod (cd49640.producer/make "in-test"))

  ;; utilities
  (def base-ts 1234)
  (defn push-sample
    [id m offset-ts v]
    (cd49640.producer/push prod
                           (new cd49640.Sample id m (+ base-ts offset-ts) v)))
  (defn stop
    []
    (.close streams)
    (cd49640.producer/close prod))

  ;; push samples
  (push-sample "id0" "a" 0 10.0)
  (push-sample "id0" "b" 0 1.0)

  ;; push a million samples
  ;; size before
  ;; root@09aa0d2a65eb:/var/lib/kafka/data# du -sh
  ;; 24K	.
  ;; size after
  ;; root@09aa0d2a65eb:/var/lib/kafka/data# du -sh
  ;; 211M	.
  (->> (* 1000 1000)
       range
       (map (fn [t] (as-> t $
                      (+ 1234 $)
                      (push-sample "id0" "a" $ 1.0))))
       dorun)
  )
